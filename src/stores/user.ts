import { ref, computed } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";

export const useUserStore = defineStore("user", () => {
  const dialog = ref(false);
  const lastId = 4;
  const users = ref<User[]>([
    { id: 1, login: "admin", name: "Adminstrator", password: "pass@1234" },
    { id: 2, login: "admin1", name: "Usre1", password: "pass@1234" },
    { id: 3, login: "admin2", name: "Usre2", password: "pass@1234" },
  ]);

  const deleteUser = (id: number): void => {
    const index = users.value.findIndex((item) => item.id === id);
    users.value.splice(index, 1);
  };
  return { users, deleteUser, dialog };
});
